from rest_framework import routers
from .views import DrugApiView, PharmacyApiView, PrescriptionApiView, BookingApiView

router = routers.DefaultRouter()

router.register('drug', DrugApiView, basename='drug-api')
router.register('pharmacy', PharmacyApiView, basename='pharmacy-api')
router.register('prescription', PrescriptionApiView, basename='prescription-api')
router.register('booking', BookingApiView, basename='booking-api')
