from rest_framework import serializers
from ..models import Drug, PrescriptionDrug, Prescription, Pharmacy, Booking, AvailabilitySchedule
from django.contrib.auth import get_user_model
import datetime


class DrugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drug
        fields = "__all__"


class AvailabilityScheduleSerializer(serializers.ModelSerializer):
    day = serializers.SerializerMethodField()

    def get_day(self, obj):
        return obj.get_day_display()

    class Meta:
        model = AvailabilitySchedule
        fields = ['day', 'from_time', 'to_time']


class PharmacySerializer(serializers.ModelSerializer):
    availability_schedule = AvailabilityScheduleSerializer(many=True, read_only=True,
                                                           source="availabilityschedule_set")

    class Meta:
        model = Pharmacy
        fields = ['id', 'name', 'availability_schedule']


class PrescriptionDrugSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrescriptionDrug
        fields = ['id', 'drug', 'quantity']
        read_only_fields = ['id']


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = ['pharmacy', 'scheduled_datetime', 'created_datetime']

    def create(self, validated_data):
        pharmacy = validated_data["pharmacy"]
        scheduled_datetime = validated_data["scheduled_datetime"]
        if scheduled_datetime < datetime.datetime.now(datetime.timezone.utc):
            raise serializers.ValidationError(
                f"You cant select a past date")
        day_no = scheduled_datetime.weekday()
        availability_schedule = pharmacy.availabilityschedule_set.filter(day=day_no).first()
        if not availability_schedule:
            raise serializers.ValidationError(
                f"'{pharmacy.name}' is not available in the selected day {scheduled_datetime}")
        if availability_schedule.from_time > scheduled_datetime.time():
            raise serializers.ValidationError(
                f"'{pharmacy.name}' starts at {availability_schedule.from_time}")
        if availability_schedule.to_time < scheduled_datetime.time():
            raise serializers.ValidationError(
                f"'{pharmacy.name}' closes at {availability_schedule.to_time}")
        instance = Booking.objects.create(**validated_data)
        return instance


class PrescriptionSerializer(serializers.ModelSerializer):
    prescription_drugs = PrescriptionDrugSerializer(many=True, source='prescriptiondrug_set')
    booking = BookingSerializer(many=False, read_only=True)

    class Meta:
        model = Prescription
        fields = ['id', 'prescription_drugs', 'date_created', 'booking']

    def create(self, validated_data):
        prescription_drugs_data = validated_data.pop('prescriptiondrug_set')
        prescription = Prescription.objects.create(**validated_data)
        for prescription_drug in prescription_drugs_data:
            PrescriptionDrug.objects.create(prescription=prescription, **prescription_drug)
        return prescription

    def update(self, instance, validated_data):
        prescription_drugs_data = validated_data.pop('prescriptiondrug_set')
        for prescription_drug_data in prescription_drugs_data:
            prescription_drug = instance.prescriptiondrug_set.filter(drug=prescription_drug_data.get('drug')).first()
            if prescription_drug:
                if prescription_drug_data['quantity'] == 0:
                    prescription_drug.delete()
                else:
                    prescription_drug.quantity = prescription_drug_data['quantity']
                    prescription_drug.save()
            else:
                PrescriptionDrug.objects.create(prescription=instance, **prescription_drug_data)
        return instance


class PrescriptionLimitedSerializer(serializers.ModelSerializer):
    prescription_drugs = PrescriptionDrugSerializer(many=True, source='prescriptiondrug_set')

    class Meta:
        model = Prescription
        fields = ['id', 'prescription_drugs', 'date_created', 'user']


class BookingDetailSerializer(serializers.ModelSerializer):
    prescription = PrescriptionLimitedSerializer(read_only=True, many=False)

    class Meta:
        model = Booking
        fields = ['pharmacy', 'scheduled_datetime', 'created_datetime', "prescription"]
