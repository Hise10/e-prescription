from rest_framework import status, viewsets, generics, mixins
from .serializer import DrugSerializer, PharmacySerializer, PrescriptionSerializer, BookingSerializer, BookingDetailSerializer
from ..models import Drug, Pharmacy, Prescription, Booking
from rest_framework.decorators import action
from rest_framework.response import Response
import coreapi
import coreschema
from rest_framework.schemas import ManualSchema, AutoSchema
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist


class DrugApiView(viewsets.ReadOnlyModelViewSet):
    model = Drug
    serializer_class = DrugSerializer
    queryset = Drug.objects.all()


class PharmacyApiView(viewsets.ReadOnlyModelViewSet):
    model = Pharmacy
    serializer_class = PharmacySerializer
    queryset = Pharmacy.objects.all()


booking_schema = ManualSchema(
    fields=[
        coreapi.Field(
            "id",
            required=True,
            location="path",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "pharmacy",
            required=True,
            location="form",
            schema=coreschema.Number()
        ),
        coreapi.Field(
            "scheduled_datetime",
            required=True,
            location="form",
            description='YYYY-MM-DDTHH:mm format.',
            schema=coreschema.String()
        )
    ], description="scheduled_datetime YYYY-MM-DDTHH:mm format.")


class PrescriptionApiView(viewsets.ModelViewSet):
    """
        list:
            list of all the existing users Prescription for that user.

        create:
            Params: prescriptiondrug_set: list of prescriptiondrug
            <br>
            <b>prescriptiondrug takes drug:int, quantity:int</b>\n
            return 201 created
        update:
            Params: prescriptiondrug_set: list of prescriptiondrug
            <br>
            <b>prescriptiondrug takes drug:int, quantity:int</b>
            <br>
            updates the data of prescription,  if the same drug in the prescription
            it will update the quantity,  if the quantity is set to zero it
            will delete the  prescriptiondrug instance, if the drug is not present
            it will create a new instance of prescriptiondrug.\n
            return 201 created


        """

    model = Prescription
    serializer_class = PrescriptionSerializer

    def get_queryset(self):
        return Prescription.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(detail=True, methods=['post'], schema=booking_schema)
    def booking(self, request, pk):
        """ params: scheduled_datetime, pharmacy """
        prescription = self.get_object()
        serializer = BookingSerializer(data=request.data)
        if serializer.is_valid():
            try:
                booking = serializer.save(prescription=prescription)
                return Response({'booking': serializer.data}, status=status.HTTP_201_CREATED)
            except IntegrityError:
                return Response({'error': "You have already booked this prescription, please delete first"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['delete'], url_path='booking/delete',
            description="returns 202 for deleted"
                        "returns 404 if there is no booking")
    def delete_booking(self, request, pk):
        prescription = self.get_object()
        try:
            if prescription.booking:
                prescription.booking.delete()
                return Response({"detail": "your booking has been deleted"}, status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist:
            return Response({"error": "you have no bookings"}, status=status.HTTP_404_NOT_FOUND)


class BookingApiView(viewsets.ReadOnlyModelViewSet):
    model = Booking
    serializer_class = BookingDetailSerializer
    queryset = Booking.objects.all()