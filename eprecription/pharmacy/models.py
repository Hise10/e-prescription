from django.db import models
from django.contrib.auth import get_user_model
import uuid
# Create your models here.


class Pharmacy(models.Model):
    name = models.CharField(max_length=120)


class AvailabilitySchedule(models.Model):
    """
    Day is the same as datetime as not to make any confusion

    """
    class Day(models.IntegerChoices):
        monday = 0
        tuesday = 1
        wednesday = 2
        thursday = 3
        friday = 4
        saturday = 5
        sunday = 6

    pharmacy = models.ForeignKey(Pharmacy, on_delete=models.CASCADE, db_index=True)
    day = models.IntegerField(choices=Day.choices, db_index=True)
    from_time = models.TimeField()
    to_time = models.TimeField()


class Drug(models.Model):
    name = models.CharField(max_length=120)


class Prescription(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)


class PrescriptionDrug(models.Model):
    drug = models.ForeignKey(Drug, on_delete=models.CASCADE)
    prescription = models.ForeignKey(Prescription, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    detail = models.CharField(max_length=255)

    class Meta:
        unique_together = ['drug', "prescription"]


class Booking(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    prescription = models.OneToOneField(Prescription, on_delete=models.CASCADE)
    pharmacy = models.ForeignKey(Pharmacy, on_delete=models.CASCADE)
    scheduled_datetime = models.DateTimeField()
    created_datetime = models.DateTimeField(auto_now_add=True)
