from django.contrib import admin
from .models import Booking, AvailabilitySchedule, Prescription, Pharmacy, PrescriptionDrug, Drug
# Register your models here.

admin.site.register(AvailabilitySchedule)
admin.site.register(Booking)
admin.site.register(Prescription)
admin.site.register(Pharmacy)
admin.site.register(PrescriptionDrug)
admin.site.register(Drug)
