from rest_framework import permissions


class IsUser(permissions.BasePermission):
    """
    gives permissions only for the same user
    """
    def has_object_permission(self, request, view, obj):
        try:
            return obj.user == request.user
        except AttributeError as e:
            return obj == request.user
