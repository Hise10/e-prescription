from rest_framework import status, viewsets, generics, mixins
from .serializer import UserSerializer
from .permissions import IsUser
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated


class UserApiView(generics.RetrieveUpdateAPIView):
    # permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self):
        return get_user_model().objects.all()

    def get_object(self):
        return self.request.user




