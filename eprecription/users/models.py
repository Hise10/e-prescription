from django.db import models
import uuid
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
import datetime
from .managers import CustomUserManager
# Create your models here.


class CustomUser(AbstractUser):
    """ Custom user model class"""
    class Gender(models.IntegerChoices):
        Male = 0
        Female = 1

    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    is_doctor = models.BooleanField(default=False)
    date_of_birth = models.DateField()
    gender = models.IntegerField(choices=Gender.choices, default=Gender.Male)

    REQUIRED_FIELDS = ['email', 'date_of_birth']

    objects = CustomUserManager()

    @property
    def age(self):
        td = datetime.date.today() - self.date_of_birth
        return int(td.days / 365.25)

    def __str__(self):
        return self.email

#
# class PhysicianSpecialty(models.Model):
#     type = models.CharField(max_length=100)
#
#     def __str__(self):
#         return self.type
#
#
# class DoctorProfile(models.Model):
#     user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
#     degree_1 = models.CharField(max_length=64)
#     degree_2 = models.CharField(max_length=64)
#     physician_specialty = models.ManyToManyField(PhysicianSpecialty)
#


