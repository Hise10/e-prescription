from django.contrib import admin
from .models import CustomUser#, DoctorProfile, PhysicianSpecialty, AvailabilitySchedule
from django.contrib.auth.admin import UserAdmin


class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ('username', 'email', 'is_staff', 'is_active', 'is_doctor', 'date_joined')
    list_filter = ('email', 'is_staff', 'is_active', 'is_doctor',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_staff', 'is_active', 'is_doctor',
                                    'is_superuser', 'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active',  'is_superuser', 'is_doctor')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)


# Register your models here.
admin.site.register(CustomUser, CustomUserAdmin)
# admin.site.register(DoctorProfile)
# admin.site.register(PhysicianSpecialty)
# admin.site.register(AvailabilitySchedule)